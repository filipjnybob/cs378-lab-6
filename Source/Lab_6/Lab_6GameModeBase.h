// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Lab_6GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LAB_6_API ALab_6GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
