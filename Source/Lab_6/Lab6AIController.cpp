// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab6AIController.h"
#include "Lab6AI.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"

ALab6AIController::ALab6AIController() {
	BehaviorComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorTreeComponent"));
	BlackboardComponent = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComponent"));

	CurrentPatrolPoint = 0;
	NextPointKey = "NextPoint";
}

void ALab6AIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	ALab6AI* AICharacter = Cast<ALab6AI>(InPawn);

	if (BlackboardComponent) {
		if (AICharacter && AICharacter->GetBehaviorTree()) {
			if (AICharacter->GetBehaviorTree()->BlackboardAsset) {
				BlackboardComponent->InitializeBlackboard(*(AICharacter->GetBehaviorTree()->BlackboardAsset));
			}

			PatrolPoints = AICharacter->GetPatrolPoints();
			BlackboardComponent->SetValueAsObject(NextPointKey, PatrolPoints[CurrentPatrolPoint]);

			if (BehaviorComponent) {
				BehaviorComponent->StartTree(*AICharacter->GetBehaviorTree());
			}
		}
	}
}
