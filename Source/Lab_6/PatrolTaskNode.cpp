// Fill out your copyright notice in the Description page of Project Settings.


#include "PatrolTaskNode.h"
#include "Lab6AIController.h"
#include "PatrolPoint.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UPatrolTaskNode::ExecuteTask(UBehaviorTreeComponent& OwnerComponent, uint8* NodeMemory)
{
	ALab6AIController* AIController = Cast<ALab6AIController>(OwnerComponent.GetAIOwner());

	if (AIController) {
		UBlackboardComponent* BlackboardComponent = AIController->GetBlackboardComponent();

		TArray<AActor*> PatrolPoints = AIController->GetPatrolPoints();

		APatrolPoint* NextPoint = Cast<APatrolPoint>(PatrolPoints[0]);

		int32 CurrentPatrolPoint = AIController->GetCurrentPatrolPoint() + 1;

		if (CurrentPatrolPoint >= PatrolPoints.Num()) {
			CurrentPatrolPoint = 0;
		}
		NextPoint = Cast<APatrolPoint>(PatrolPoints[CurrentPatrolPoint]);
		AIController->SetCurrentPatrolPoint(CurrentPatrolPoint);

		BlackboardComponent->SetValueAsObject(AIController->NextPointKey, NextPoint);

		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Failed;
}
