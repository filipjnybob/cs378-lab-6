// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Lab6AIController.generated.h"

/**
 * 
 */
UCLASS()
class LAB_6_API ALab6AIController : public AAIController
{
	GENERATED_BODY()
	
public:
	ALab6AIController();

	FORCEINLINE class UBlackboardComponent* GetBlackboardComponent() const { return BlackboardComponent; }
	FORCEINLINE TArray<AActor*> GetPatrolPoints() const { return PatrolPoints; }
	FORCEINLINE int32 GetCurrentPatrolPoint() const { return CurrentPatrolPoint; }

	UFUNCTION()
		void SetCurrentPatrolPoint(int32 NewPatrolPoint) { CurrentPatrolPoint = NewPatrolPoint; }

	UPROPERTY(EditDefaultsOnly)
		FName NextPointKey;

protected:
	UPROPERTY()
	class UBehaviorTreeComponent* BehaviorComponent;

	UPROPERTY()
	class UBlackboardComponent* BlackboardComponent;

	UPROPERTY()
	int32 CurrentPatrolPoint;

	UPROPERTY()
	TArray<AActor*> PatrolPoints;

	virtual void OnPossess(APawn* InPawn) override;
};
